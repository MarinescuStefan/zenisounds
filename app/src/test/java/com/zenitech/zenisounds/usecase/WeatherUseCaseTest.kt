package com.zenitech.zenisounds.usecase

import com.zenitech.zenisounds.model.*
import com.zenitech.zenisounds.repository.LocationRepository
import com.zenitech.zenisounds.repository.WeatherRepository
import com.zenitech.zenisounds.util.Failure
import com.zenitech.zenisounds.util.Success
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.lang.IllegalStateException

class WeatherUseCaseTest {

    private val locationRepository: LocationRepository = mockk()
    private val weatherRepository: WeatherRepository = mockk()
    private val failureResult = Failure(IllegalStateException(""))
    private val defaultWeatherModel = WeatherModel(Weather.CLEAR, Temperature.WARM, "01")
    private lateinit var sut: WeatherUseCase

    @Before
    fun setup() {
        sut = spyk(WeatherUseCase(weatherRepository, locationRepository), recordPrivateCalls = true)
    }


    @Test
    fun `given getLocation() fails, when getWeather() is called verify default weather is returned`() =
        runBlocking {
            //given
            coEvery { locationRepository.getLocation() } returns failureResult
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel
            //when
            val result = sut.getWeather()

            //then
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 0) { weatherRepository.getWeather(any(), any()) }
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            assert(result == defaultWeatherModel)
        }


    @Test
    fun `given location request is successful but latitude response is invalid, verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel
            every { locationResponse.latitude } returns null
            every { locationResponse.longitude } returns 10F

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 0) { weatherRepository.getWeather(any(), any()) }
            assert(result == defaultWeatherModel)
        }

    @Test
    fun `given location request is successful but longitude response is invalid, verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns null

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 0) { weatherRepository.getWeather(any(), any()) }
            assert(result == defaultWeatherModel)
        }

    @Test
    fun `given location response is successful but weather response is invalid,verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            val weatherApiResponse = WeatherAPIResponse(listOf())
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns 10F
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            coEvery { weatherRepository.getWeather(10F, 10F) } returns Success(weatherApiResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 1) { weatherRepository.getWeather(10F, 10F) }
            assert(result == defaultWeatherModel)
        }

    @Test
    fun `given location response is successful and weather response is successful but invalid, verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            val weatherApiResponse = WeatherAPIResponse(listOf())
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns 10F
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            coEvery { weatherRepository.getWeather(10F, 10F) } returns Success(weatherApiResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 1) { weatherRepository.getWeather(10F, 10F) }
            assert(result == defaultWeatherModel)
        }


    @Test
    fun `given location response is successful and weather response is failure,verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            val weatherApiResponse = WeatherAPIResponse(listOf(WeatherList(MainWeather(10F),weather = listOf())))
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns 10F
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            coEvery { weatherRepository.getWeather(10F, 10F) } returns failureResult
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 1) { weatherRepository.getWeather(10F, 10F) }
            assert(result == defaultWeatherModel)
        }

    @Test
    fun `given location response is successful and weather response is successful but invalid,verify default weather is returned`() =
        runBlocking {
            //given
            val locationResponse = mockk<LocationDTO>()
            val weatherApiResponse = WeatherAPIResponse(listOf(WeatherList(MainWeather(10F),weather = listOf())))
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns 10F
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            coEvery { weatherRepository.getWeather(10F, 10F) } returns Success(weatherApiResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 1) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 1) { weatherRepository.getWeather(10F, 10F) }
            assert(result == defaultWeatherModel)
        }

    @Test
    fun `given location response is successful and weather response is successful but invalid, verify correct return value`() =
        runBlocking {
            //given
            val icon = "02d"
            val weatherModel = WeatherModel(Weather.CLOUDS,Temperature.WARM,icon)
            val locationResponse = mockk<LocationDTO>()
            val weatherApiResponse = WeatherAPIResponse(listOf(WeatherList(MainWeather(10F),weather = listOf(
                WeatherDTO(801,icon)
            ))))
            every { locationResponse.latitude } returns 10F
            every { locationResponse.longitude } returns 10F
            coEvery { locationRepository.getLocation() } returns Success(locationResponse)
            coEvery { weatherRepository.getWeather(10F, 10F) } returns Success(weatherApiResponse)
            every { sut["getDefaultWeather"]() } returns defaultWeatherModel

            //when
            val result = sut.getWeather()

            //then
            verify(exactly = 0) { sut["getDefaultWeather"]() }
            coVerify(exactly = 1) { locationRepository.getLocation() }
            coVerify(exactly = 1) { weatherRepository.getWeather(10F, 10F) }

            assert(result == weatherModel)
        }


}