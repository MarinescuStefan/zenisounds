package com.zenitech.zenisounds.usecase

import com.zenitech.zenisounds.R
import com.zenitech.zenisounds.provider.CoroutineContextProvider
import com.zenitech.zenisounds.ui.main.MoodUiModel
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MoodUseCaseTest {

    private val weatherUseCaseTest: WeatherUseCase = mockk()
    private val coroutineContextProvider: CoroutineContextProvider = spyk()
    private lateinit var sut: MoodUseCase

    @Before
    fun setup() {
        sut = MoodUseCase(coroutineContextProvider, weatherUseCaseTest)
    }

    @Test
    fun `given clear weather and warm temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.CLEAR,Temperature.WARM,"01d")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.WARM_CLEAR.key, R.drawable.clear))
    }

    @Test
    fun `given hot weather and thunderstorm temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.THUNDERSTORM,Temperature.HOT,"04n")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.SUMMER_THUNDERSTORM.key, R.drawable.clouds_night))
    }


    @Test
    fun `given snow weather and frozen temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.SNOW,Temperature.FROZEN,"13d")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.ICY_SNOW.key, R.drawable.snow))
    }

    @Test
    fun `given rain weather and cold temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.RAIN,Temperature.COLD,"11n")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.FROZEN_RAIN.key, R.drawable.rain_night))
    }

    @Test
    fun `given clouds weather and warm temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.CLOUDS,Temperature.WARM,"02d")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.WARM_CLOUDS.key, R.drawable.cloud))
    }

    @Test
    fun `given clear weather and cold temperature, when getMood() is called, verify return result`() = runBlocking {
        //given
        coEvery { weatherUseCaseTest.getWeather() } returns WeatherModel(Weather.CLEAR,Temperature.COLD,"01n")

        //when
        val result = sut.getMood()

        //then
        assert(result == MoodUiModel(Mood.FROZEN_CLEAR.key, R.drawable.clear_night))
    }

}