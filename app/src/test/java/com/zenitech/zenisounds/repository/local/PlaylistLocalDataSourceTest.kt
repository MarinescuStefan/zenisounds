package com.zenitech.zenisounds.repository.local

import com.zenitech.zenisounds.database.dao.PlaylistDao
import com.zenitech.zenisounds.database.entity.Playlist
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class PlaylistLocalDataSourceTest {

    private lateinit var sut : PlaylistLocalDataSource

    @Before
    fun init(){
        sut = PlaylistLocalDataSource(mockk())
    }

    @Test
    fun `when getPlaylists() is called, verify return result and call is forwarded to dao`() = runBlocking {
        //given
        val playlistMock : List<Playlist> = mockk()
        coEvery { sut.getPlaylists() } returns playlistMock

        //when
        val result = sut.getPlaylists()

        //then
        assertTrue(result == playlistMock)
        coVerify(exactly = 1) { sut.getPlaylists() }
    }


}

