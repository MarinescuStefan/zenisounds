package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.model.WeatherAPIResponse
import com.zenitech.zenisounds.repository.remote.WeatherRemoteDataSource
import com.zenitech.zenisounds.util.Result
import com.zenitech.zenisounds.util.Success
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class WeatherRepositoryTest {

    private lateinit var sut: WeatherRepository
    private val weatherRemoteDataSource: WeatherRemoteDataSource = mockk()

    @Before
    fun setup() {
        sut = WeatherRepository(weatherRemoteDataSource)
    }

    @Test
    fun `given longitude and latitude, when getWeather() is called, verify parameters are passed to remote data source`() =
        runBlocking {
            //given
            val latitude = 10F
            val longitude = 10F
            val weatherApiResponse: Result<WeatherAPIResponse> = Success(mockk())
            coEvery {
                weatherRemoteDataSource.getWeather(
                    latitude,
                    longitude
                )
            } returns weatherApiResponse

            //when
            val result = sut.getWeather(latitude, longitude)

            //then
            assertTrue(result == weatherApiResponse)
            coVerify(exactly = 1) { weatherRemoteDataSource.getWeather(latitude, longitude) }
        }


}