package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.database.entity.Playlist
import com.zenitech.zenisounds.repository.local.PlaylistLocalDataSource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class PlaylistRepositoryTest {

    private lateinit var sut : PlaylistRepository
    private val playlistLocalDataSource :PlaylistLocalDataSource = mockk()

    @Before
    fun setup(){
        sut = PlaylistRepository(playlistLocalDataSource)
    }

    @Test
    fun `when getPlaylists() is called, verify return result and call is forwarded to local data source`() = runBlocking {
        //given
        val playlistMock : List<Playlist> = mockk()
        coEvery { playlistLocalDataSource.getPlaylists() } returns playlistMock

        //when
        val result = sut.getPlaylists()

        //then
        assertTrue(result == playlistMock)
        coVerify(exactly = 1) { sut.getPlaylists() }
    }

}