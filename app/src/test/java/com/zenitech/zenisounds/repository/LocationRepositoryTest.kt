package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.model.LocationDTO
import com.zenitech.zenisounds.repository.remote.GeoLocationRemoteDataSource
import com.zenitech.zenisounds.util.Result
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class LocationRepositoryTest {

    private lateinit var sut : LocationRepository
    private val geoLocationRemoteDataSource : GeoLocationRemoteDataSource = mockk()

    @Before
    fun setup(){
        sut = LocationRepository(geoLocationRemoteDataSource)
    }

    @Test
    fun `when getLocation() is called, verify call is passed to remote data source`() = runBlocking {
        //given
        val locationMock = mockk<Result<LocationDTO>>()
        coEvery { sut.getLocation() } returns locationMock

        //when
        val result = sut.getLocation()

        //then
        assertTrue(result == locationMock)
        coVerify(exactly = 1) { geoLocationRemoteDataSource.getLocation() }
    }

}