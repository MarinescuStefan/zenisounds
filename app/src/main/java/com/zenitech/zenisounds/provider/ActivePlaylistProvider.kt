package com.zenitech.zenisounds.provider

import com.google.android.exoplayer2.SimpleExoPlayer
import com.zenitech.zenisounds.ui.playlist.VolumeChange
import javax.inject.Singleton

@Singleton
class ActivePlaylistProvider {
    private val activePlaylist = mutableMapOf<Int, SimpleExoPlayer>()

    fun stopPlayer() = activePlaylist.values.forEach { it.release() }

    fun isPlayerRunning() = activePlaylist.values.first().isPlaying

    fun addPlayer(id: Int, player: SimpleExoPlayer) {
        activePlaylist[id] = player
    }

    fun changeVolume(it: VolumeChange) {
        activePlaylist[it.soundId]?.volume = it.volume
    }

    fun toggleStartStop() {
        activePlaylist.values.forEach {
            when {
                it.isPlaying -> it.pause()
                else -> it.play()
            }
        }


    }
}