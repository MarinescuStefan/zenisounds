package com.zenitech.zenisounds.provider

import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext


private val loggingExceptionHandler = CoroutineExceptionHandler { _, throwable ->
    Timber.e(throwable)
    throw throwable
}

open class CoroutineContextProvider {
    open val Main: CoroutineContext by lazy { Dispatchers.Main }
    open val IO: CoroutineContext by lazy { Dispatchers.IO }
    open val Default: CoroutineContext by lazy { Dispatchers.Default }
}

internal fun CoroutineContext.launch(
    job: CompletableJob = Job(),
    block: suspend CoroutineScope.() -> Unit
): Job =
    CoroutineScope(context = this + job + loggingExceptionHandler).launch(block = block)

