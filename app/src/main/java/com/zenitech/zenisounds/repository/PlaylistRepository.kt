package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.repository.local.PlaylistLocalDataSource
import com.zenitech.zenisounds.usecase.Mood
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistRepository @Inject constructor(private val playlistLocalDataSource: PlaylistLocalDataSource) {

    suspend fun getPlaylists() = playlistLocalDataSource.getPlaylists()

    suspend fun getPlaylistForMood(mood: Mood) = playlistLocalDataSource.getPLaylistForMood(mood)
}