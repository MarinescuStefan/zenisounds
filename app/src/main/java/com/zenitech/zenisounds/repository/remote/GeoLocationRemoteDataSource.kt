package com.zenitech.zenisounds.repository.remote

import com.zenitech.zenisounds.service.GeolocationService
import com.zenitech.zenisounds.util.safeApiCall
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GeoLocationRemoteDataSource @Inject constructor(private val geolocationService: GeolocationService) {

    suspend fun getLocation() = safeApiCall("Failed to retrieve location"){
        geolocationService.getUserLocation()
    }
}