package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.repository.remote.WeatherRemoteDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepository @Inject constructor(
    private val weatherRemoteDataSource: WeatherRemoteDataSource
) {
    suspend fun getWeather(latitude: Float, longitude: Float) =
        weatherRemoteDataSource.getWeather(latitude, longitude)

}