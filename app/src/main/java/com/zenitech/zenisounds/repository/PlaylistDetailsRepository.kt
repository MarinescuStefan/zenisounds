package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.repository.local.PlaylistDetailsLocalDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistDetailsRepository @Inject constructor(private val playlistDetailsLocalDataSource : PlaylistDetailsLocalDataSource) {

    suspend fun getPlaylistDetails(playlistId: Int) = playlistDetailsLocalDataSource.getPlaylistDetails(playlistId)

    suspend fun getPlaylistName(playlistId: Int) = playlistDetailsLocalDataSource.getPlaylistName(playlistId)
}