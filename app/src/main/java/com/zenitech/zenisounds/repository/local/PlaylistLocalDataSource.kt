package com.zenitech.zenisounds.repository.local

import com.zenitech.zenisounds.database.dao.PlaylistDao
import com.zenitech.zenisounds.database.entity.PlaylistType
import com.zenitech.zenisounds.usecase.Mood
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistLocalDataSource @Inject constructor(private val playlistDao: PlaylistDao) {

    suspend fun getPlaylists() = playlistDao.getPlaylists()

    suspend fun getPLaylistForMood(mood: Mood) = playlistDao.getPLaylistForMood(mood.key)
}