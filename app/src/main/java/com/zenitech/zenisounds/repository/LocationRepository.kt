package com.zenitech.zenisounds.repository

import com.zenitech.zenisounds.repository.remote.GeoLocationRemoteDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationRepository @Inject constructor(
    private val geoLocationRemoteDataSource: GeoLocationRemoteDataSource
) {

    suspend fun getLocation() = geoLocationRemoteDataSource.getLocation()

}