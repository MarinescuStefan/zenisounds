package com.zenitech.zenisounds.repository.remote

import com.zenitech.zenisounds.service.GeolocationService
import com.zenitech.zenisounds.service.WeatherService
import com.zenitech.zenisounds.util.safeApiCall
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRemoteDataSource @Inject constructor(
    private val weatherService: WeatherService,
    private val geolocationService: GeolocationService
) {

    suspend fun getWeather(latitude: Float, longitude: Float) = safeApiCall(""){
        weatherService.getWeather(latitude,longitude)
    }

}