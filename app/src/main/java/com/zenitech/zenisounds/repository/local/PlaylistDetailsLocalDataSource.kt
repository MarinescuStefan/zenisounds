package com.zenitech.zenisounds.repository.local

import com.zenitech.zenisounds.database.dao.PlaylistXRefSoundDao
import com.zenitech.zenisounds.database.dao.SoundUiModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistDetailsLocalDataSource @Inject constructor(private val playlistXRefSoundDao: PlaylistXRefSoundDao) {

    suspend fun getPlaylistDetails(playlistId: Int) = playlistXRefSoundDao.getPlaylistDetails(playlistId)

    suspend fun getPlaylistName(playlistId: Int) = playlistXRefSoundDao.getPlaylistName(playlistId)

}