package com.zenitech.zenisounds.usecase

import androidx.annotation.DrawableRes
import com.zenitech.zenisounds.R
import com.zenitech.zenisounds.provider.CoroutineContextProvider
import com.zenitech.zenisounds.ui.main.MoodUiModel
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoodUseCase @Inject constructor(
    private val coroutineContextProvider: CoroutineContextProvider,
    private val weatherUseCase: WeatherUseCase
) {
    val mood: AtomicReference<Mood> = AtomicReference(Mood.WARM_CLOUDS)

    suspend fun getMood(): MoodUiModel = withContext(coroutineContextProvider.IO) {
        val weather = weatherUseCase.getWeather()
        val mood = getMood(weather.temperature, weather.weather)
        this@MoodUseCase.mood.set(mood)
        val moodTitle = mood.key
        val imageRes = getImage(weather.icon)
        MoodUiModel(
            moodTitle,
            imageRes
        )
    }


    @DrawableRes
    private fun getImage(icon: String): Int {
        val timeOfDay = if (icon.contains("n")) TimeOfDay.NIGHT else TimeOfDay.DAY
        return when {
            clear.any { icon.contains(it) } -> if (timeOfDay == TimeOfDay.DAY) R.drawable.clear else R.drawable.clear_night
            clouds.any { icon.contains(it) } -> if (timeOfDay == TimeOfDay.DAY) R.drawable.cloud else R.drawable.clouds_night
            rain.any { icon.contains(it) } -> if (timeOfDay == TimeOfDay.DAY) R.drawable.rain else R.drawable.rain_night
            snow.any { icon.contains(it) } -> if (timeOfDay == TimeOfDay.DAY) R.drawable.snow else R.drawable.snow_night
            else -> R.drawable.cloud
        }
    }

    enum class TimeOfDay {
        DAY,
        NIGHT
    }

    private fun getMood(condition: Temperature, weatherType: Weather): Mood =
        when {
            //thunderstorm
            condition == Temperature.FROZEN && weatherType == Weather.THUNDERSTORM -> Mood.ICY_THUNDERSTORM
            condition == Temperature.COLD && weatherType == Weather.THUNDERSTORM -> Mood.FROZEN_THUNDERSTORM
            condition == Temperature.WARM && weatherType == Weather.THUNDERSTORM -> Mood.DAILY_THUNDERSTORM
            condition == Temperature.HOT && weatherType == Weather.THUNDERSTORM -> Mood.SUMMER_THUNDERSTORM

            //rain
            condition == Temperature.FROZEN && weatherType == Weather.RAIN -> Mood.ICY_RAIN
            condition == Temperature.COLD && weatherType == Weather.RAIN -> Mood.FROZEN_RAIN
            condition == Temperature.WARM && weatherType == Weather.RAIN -> Mood.LIGHT_RAIN
            condition == Temperature.HOT && weatherType == Weather.RAIN -> Mood.SUMMER_RAIN

            //snow
            condition == Temperature.FROZEN && weatherType == Weather.SNOW -> Mood.ICY_SNOW
            condition == Temperature.COLD && weatherType == Weather.SNOW -> Mood.FROZEN_SNOW
            condition == Temperature.WARM && weatherType == Weather.SNOW -> Mood.LIGHT_SNOW
            condition == Temperature.HOT && weatherType == Weather.SNOW -> Mood.LIGHT_SNOW

            //Mist
            condition == Temperature.FROZEN && weatherType == Weather.MIST -> Mood.ICY_MIST
            condition == Temperature.COLD && weatherType == Weather.MIST -> Mood.FROZEN_MIST
            condition == Temperature.WARM && weatherType == Weather.MIST -> Mood.WARM_MIST
            condition == Temperature.HOT && weatherType == Weather.MIST -> Mood.HOT_MIST

            //clouds
            condition == Temperature.FROZEN && weatherType == Weather.CLOUDS -> Mood.ICY_CLOUDS
            condition == Temperature.COLD && weatherType == Weather.CLOUDS -> Mood.FROZEN_CLOUDS
            condition == Temperature.WARM && weatherType == Weather.CLOUDS -> Mood.WARM_CLOUDS
            condition == Temperature.HOT && weatherType == Weather.CLOUDS -> Mood.HOT_CLOUDS

            //clear
            condition == Temperature.FROZEN && weatherType == Weather.CLEAR -> Mood.ICY_CLEAR
            condition == Temperature.COLD && weatherType == Weather.CLEAR -> Mood.FROZEN_CLEAR
            condition == Temperature.WARM && weatherType == Weather.CLEAR -> Mood.WARM_CLEAR
            condition == Temperature.HOT && weatherType == Weather.CLEAR -> Mood.HOT_CLEAR


            else -> Mood.WARM_CLEAR
        }

    companion object {
        val clear: List<String> = listOf("01")
        val clouds: List<String> = listOf("02", "03", "04")
        val rain: List<String> = listOf("09", "10", "11")
        val snow: List<String> = listOf("13")
    }
}

enum class Mood(val key: String) {
    //thunderstorm playlists
    ICY_THUNDERSTORM("Icy Thunderstorm"),
    FROZEN_THUNDERSTORM("Frozen Thunderstorm"),
    DAILY_THUNDERSTORM("Powerful Thunders"),
    SUMMER_THUNDERSTORM("Hot Thunders"),

    //rain,drizzle playlists
    ICY_RAIN("Icy Rain"),
    FROZEN_RAIN("Coooold Rain"),
    LIGHT_RAIN("Lil' Rain"),
    SUMMER_RAIN("Summer Rain"),


    //snowPlaylist
    ICY_SNOW("Absolutely Frozen"),
    FROZEN_SNOW("Winter Time"),
    LIGHT_SNOW("Lil' Snow"),

    //mist
    ICY_MIST("Icy Conditions"),
    FROZEN_MIST("Frozen Conditions"),
    WARM_MIST("Warm Conditions"),
    HOT_MIST("Hot Conditions"),

    //clear
    ICY_CLEAR("Ice ice ice"),
    FROZEN_CLEAR("Nice, but frozen"),
    WARM_CLEAR("Perfect Time"),
    HOT_CLEAR("Summer Timeeee"),

    //clouds
    ICY_CLOUDS("Clouds of ice"),
    FROZEN_CLOUDS("Frozen Clouds"),
    WARM_CLOUDS("Cloudy"),
    HOT_CLOUDS("Hot n' cloudy");

}




