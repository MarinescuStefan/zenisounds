package com.zenitech.zenisounds.usecase

import com.google.android.exoplayer2.SimpleExoPlayer
import com.zenitech.zenisounds.database.dao.SoundUiModel
import com.zenitech.zenisounds.provider.CoroutineContextProvider
import com.zenitech.zenisounds.repository.PlaylistDetailsRepository
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistDetailsUseCase @Inject constructor(
    private val coroutineContextProvider: CoroutineContextProvider,
    private val playlistDetailsRepository: PlaylistDetailsRepository
) {

    suspend fun getPlaylistSounds(playlistId: Int) = withContext(coroutineContextProvider.IO) {
        val sounds = playlistDetailsRepository.getPlaylistDetails(playlistId).map {
            SoundUiModel(
                sound = it.sound,
                volume = it.volume ?: (2..8).random().toFloat() / 10
            )
        }
        val playlistName = playlistDetailsRepository.getPlaylistName(playlistId)
        PlaylistDetailsUiModel(
            sounds, playlistName
        )
    }
}

data class PlaylistDetailsUiModel(
    val sounds: List<SoundUiModel>,
    val playlistName: String
)