package com.zenitech.zenisounds.usecase

import com.zenitech.zenisounds.repository.LocationRepository
import com.zenitech.zenisounds.repository.WeatherRepository
import com.zenitech.zenisounds.util.Failure
import com.zenitech.zenisounds.util.Success
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.ceil

@Singleton
class WeatherUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val locationRepository: LocationRepository
) {

    suspend fun getWeather(): WeatherModel =
        when (val result = locationRepository.getLocation()) {
            is Success -> {
                if (result.data.latitude != null && result.data.longitude != null) {
                    when (val result = weatherRepository.getWeather(
                        result.data.latitude,
                        result.data.longitude
                    )) {
                        is Success -> {
                            result.data.weathers.firstOrNull()?.run {
                                WeatherModel(
                                    weather = getWeather(
                                        weather.firstOrNull()?.id ?: return@run getDefaultWeather()
                                    ),
                                    temperature = getTemperature(main.temperature),
                                    icon = weather.firstOrNull()?.icon
                                        ?: return@run getDefaultWeather()
                                )
                            } ?: getDefaultWeather()
                        }

                        is Failure -> {
                            getDefaultWeather()
                        }
                    }

                } else getDefaultWeather()


            }

            is Failure -> {
                getDefaultWeather()
            }
        }

    private fun getDefaultWeather() = WeatherModel(Weather.CLEAR, Temperature.WARM, "01d")

    private fun getTemperature(temperature: Float): Temperature {
        val roundedTemperature = ceil(temperature).toInt()
        return when {
            roundedTemperature <= 0 -> Temperature.FROZEN
            roundedTemperature in 1..9 -> Temperature.COLD
            roundedTemperature in 10..20 -> Temperature.WARM
            roundedTemperature > 20 -> Temperature.HOT
            else -> Temperature.WARM
        }
    }

    private fun getWeather(conditionId: Int) : Weather = when (conditionId) {
        in 200..233 -> Weather.THUNDERSTORM
        in 300..322 -> Weather.RAIN
        in 500..532 -> Weather.RAIN
        in 600..623 -> Weather.SNOW
        in 701..782 -> Weather.MIST
        800 -> Weather.CLEAR
        in 801..805 -> Weather.CLOUDS
        else -> Weather.CLEAR
    }


}


data class WeatherModel(
    val weather: Weather,
    val temperature: Temperature,
    val icon: String
)

enum class Temperature {
    FROZEN,
    COLD,
    WARM,
    HOT;
}

enum class Weather {
    THUNDERSTORM,
    RAIN,
    SNOW,
    MIST,
    CLEAR,
    CLOUDS;
}



