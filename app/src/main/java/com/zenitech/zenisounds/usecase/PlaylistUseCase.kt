package com.zenitech.zenisounds.usecase

import com.zenitech.zenisounds.provider.CoroutineContextProvider
import com.zenitech.zenisounds.repository.PlaylistRepository
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlaylistUseCase @Inject constructor(
    private val coroutineContextProvider: CoroutineContextProvider,
    private val playlistRepository: PlaylistRepository
) {

    suspend fun getPlaylists() = withContext(coroutineContextProvider.IO) {
        playlistRepository.getPlaylists()
    }

    suspend fun getPlayListForMood(mood: Mood) = withContext(coroutineContextProvider.IO) {
        playlistRepository.getPlaylistForMood(mood)
    }
}