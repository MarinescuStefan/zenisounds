package com.zenitech.zenisounds.util

import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import com.zenitech.zenisounds.R

object Util {

    @DrawableRes
    fun getPlaylistImage(playlistImage: PlaylistImage) = when (playlistImage) {
        PlaylistImage.BUBBLES -> R.drawable.bubbles
        PlaylistImage.CLOUDS -> R.drawable.clouds
        PlaylistImage.MOUNTAINS -> R.drawable.mountains
        PlaylistImage.OVALS -> R.drawable.ovals
        PlaylistImage.RECTANGLES -> R.drawable.rectangles
    }

    @DrawableRes
    fun getSoundImage(soundId: Int) = when (soundId) {
        1 -> R.drawable.tree
        2 -> R.drawable.train_station
        3 -> R.drawable.river
        4 -> R.drawable.rainy
        5 -> R.drawable.owl
        6 -> R.drawable.fish
        7 -> R.drawable.bonfire
        8 -> R.drawable.bird
        9 -> R.drawable.waves
        10 -> R.drawable.castle
        11 -> R.drawable.blizzard
        12 -> R.drawable.farm
        13 -> R.drawable.office
        14 -> R.drawable.traffic
        15 -> R.drawable.waterfall
        else -> R.drawable.waves
    }

    @RawRes
    fun getSound(soundId: Int) = when (soundId) {
        1 -> R.raw.forest
        2 -> R.raw.train
        3 -> R.raw.river
        4 -> R.raw.rain
        5 -> R.raw.owl
        6 -> R.raw.underwater
        7 -> R.raw.fire
        8 -> R.raw.birds
        9 -> R.raw.waves
        10 -> R.raw.medieval
        11 -> R.raw.blizzard
        12 -> R.raw.farm
        13 -> R.raw.office
        14 -> R.raw.traffic
        15 -> R.raw.waterfall
        else -> R.raw.waves
    }
}


enum class PlaylistImage {
    BUBBLES,
    CLOUDS,
    MOUNTAINS,
    OVALS,
    RECTANGLES;
}