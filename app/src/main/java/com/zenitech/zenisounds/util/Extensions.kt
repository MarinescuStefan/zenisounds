package com.zenitech.zenisounds.util

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.io.IOException


sealed class Result<out T> {

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Failure -> "Failure[error=$exception]"
        }
    }
}

data class Success<out T>(val data: T) : Result<T>()
data class Failure(
    val exception: Throwable,
    val code: Int? = null,
    val message: String? = null
) : Result<Nothing>() {

    val localizedMessage: String
        get() = if (code != null && message != null) {
            "$code : $message"
        } else {
            message ?: exception.localizedMessage ?: exception.message ?: exception.toString()
        }
}

internal suspend fun <T : Any> safeApiCall(errorMessage: String, call: suspend () -> T): Result<T> {
    return try {
        Success(call())
    } catch (e: Exception) {
        when (e) {
            // An exception was thrown when calling the API so we're converting this to an IOException
            is HttpException -> {
                e.response()?.parseError(errorMessage, e)
                    ?: Failure(IOException(errorMessage, e))

            }
            is IOException -> Failure(IOException("No internet connection", e))
            else -> {
                Timber.e("$e $errorMessage")
                Failure(e)
            }
        }
    }
}

private fun <T> Response<T>.parseError(
    message: String,
    exception: HttpException
): Failure {
    val errorBody: ResponseBody? = errorBody()
    return if (errorBody != null) { // try to get error code from the error body, if any
        val error: ErrorBody? = try {
            val jsonSerializer = Json{
                ignoreUnknownKeys = true
                isLenient = true
            }
            //jsonSerializer.decodeFromString(ErrorBody.serializer(), errorBody.string())
            ErrorBody("")
        } catch (e: SerializationException) {
            Timber.e(e)
            null
        }

        if (error == null) {
            Failure(exception, exception.code(), message)
        } else {
            Failure(exception, code(), error.message)
        }
    } else { // otherwise, return the http error code
        Failure(exception, exception.code(), message)
    }
}

@Serializable
internal data class ErrorBody(
    @SerialName("error_code")
    val code: String? = null,

    @SerialName("error_description")
    val _error_description: String? = null,

    @SerialName("message")
    val _message: String? = null,

    @SerialName("error")
    var error: String? = null
) {
    val message: String?
        get() =
            _message ?: _error_description
}