package com.zenitech.zenisounds.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.zenitech.zenisounds.BuildConfig
import com.zenitech.zenisounds.R
import com.zenitech.zenisounds.database.ZenisoundsDatabase
import com.zenitech.zenisounds.database.dao.PlaylistDao
import com.zenitech.zenisounds.database.dao.PlaylistXRefSoundDao
import com.zenitech.zenisounds.database.dao.SoundDao
import com.zenitech.zenisounds.database.entity.*
import com.zenitech.zenisounds.provider.ActivePlaylistProvider
import com.zenitech.zenisounds.provider.CoroutineContextProvider
import com.zenitech.zenisounds.provider.launch
import com.zenitech.zenisounds.service.GeolocationService
import com.zenitech.zenisounds.service.WeatherService
import com.zenitech.zenisounds.usecase.Mood
import com.zenitech.zenisounds.util.PlaylistImage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Provider
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context,
        playlistDaoProvider: Provider<PlaylistDao>,
        playlistXRefSoundDaoProvider: Provider<PlaylistXRefSoundDao>,
        soundDaoProvider: Provider<SoundDao>
    ): ZenisoundsDatabase =
        Room.databaseBuilder(
            context,
            ZenisoundsDatabase::class.java,
            context.getString(R.string.database_name)
        ).addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                CoroutineContextProvider().IO.launch {
                    playlistDaoProvider.get().run {
                        insertAll(
                            listOf(
                                Playlist(
                                    name = "Feeling Happy",
                                    image = PlaylistImage.MOUNTAINS,
                                    color = context.getColor(R.color.blueCard),
                                    type = PlaylistType.DEFAULT
                                ),
                                Playlist(
                                    name = "Focus",
                                    image = PlaylistImage.RECTANGLES,
                                    color = context.getColor(R.color.lightGreenCard),
                                    type = PlaylistType.DEFAULT
                                ),
                                Playlist(
                                    name = "Morning Coffee",
                                    image = PlaylistImage.BUBBLES,
                                    color = context.getColor(R.color.redCard),
                                    type = PlaylistType.DEFAULT
                                ),
                                Playlist(
                                    name = "Jumpstart",
                                    image = PlaylistImage.CLOUDS,
                                    color = context.getColor(R.color.lightBlueCard),
                                    type = PlaylistType.DEFAULT
                                ),
                                Playlist(
                                    name = "Sound waves",
                                    image = PlaylistImage.OVALS,
                                    color = context.getColor(R.color.greenCard),
                                    type = PlaylistType.DEFAULT
                                ),
                                Playlist(
                                    name = Mood.ICY_THUNDERSTORM.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_THUNDERSTORM.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.DAILY_THUNDERSTORM.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.SUMMER_THUNDERSTORM.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.ICY_RAIN.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_RAIN.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.LIGHT_RAIN.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.SUMMER_RAIN.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.ICY_SNOW.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_SNOW.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.LIGHT_SNOW.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.ICY_MIST.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_MIST.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.WARM_MIST.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.HOT_MIST.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.ICY_CLEAR.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_CLEAR.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.WARM_CLEAR.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.HOT_CLEAR.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.ICY_CLOUDS.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.FROZEN_CLOUDS.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.WARM_CLOUDS.key,
                                    type = PlaylistType.MOOD
                                ),
                                Playlist(
                                    name = Mood.HOT_CLOUDS.key,
                                    type = PlaylistType.MOOD
                                )
                            )

                        )
                    }

                    playlistXRefSoundDaoProvider.get().run {
                        insert(
                            //default playlists
                            PlaylistXRefSound(playlistId = 1, soundId = 1, volume = 0.6f),
                            PlaylistXRefSound(playlistId = 1, soundId = 2, volume = 0.3f),
                            PlaylistXRefSound(playlistId = 1, soundId = 10, volume = 0.8f),
                            PlaylistXRefSound(playlistId = 1, soundId = 14, volume = 0.5f),
                            PlaylistXRefSound(playlistId = 2, soundId = 1, volume = 0.6f),
                            PlaylistXRefSound(playlistId = 2, soundId = 3, volume = 0.6f),
                            PlaylistXRefSound(playlistId = 2, soundId = 6, volume = 0.2f),
                            PlaylistXRefSound(playlistId = 2, soundId = 14, volume = 0.5f),
                            PlaylistXRefSound(playlistId = 3, soundId = 5, volume = 0.7f),
                            PlaylistXRefSound(playlistId = 3, soundId = 2, volume = 0.4f),
                            PlaylistXRefSound(playlistId = 3, soundId = 11, volume = 0.9f),
                            PlaylistXRefSound(playlistId = 3, soundId = 14, volume = 0.5f),
                            PlaylistXRefSound(playlistId = 4, soundId = 9, volume = 0.9f),
                            PlaylistXRefSound(playlistId = 4, soundId = 7, volume = 0.3f),
                            PlaylistXRefSound(playlistId = 4, soundId = 6, volume = 0.5f),
                            PlaylistXRefSound(playlistId = 4, soundId = 14, volume = 0.5f),
                            PlaylistXRefSound(playlistId = 5, soundId = 10, volume = 0.7f),
                            PlaylistXRefSound(playlistId = 5, soundId = 6, volume = 0.6f),
                            PlaylistXRefSound(playlistId = 5, soundId = 1, volume = 0.4f),
                            PlaylistXRefSound(playlistId = 5, soundId = 14, volume = 0.5f),

                            //mood playlists
                            PlaylistXRefSound(playlistId = 6, soundId = 15, volume = null),
                            PlaylistXRefSound(playlistId = 6, soundId = 12, volume = null),
                            PlaylistXRefSound(playlistId = 6, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 6, soundId = 1, volume = null),

                            PlaylistXRefSound(playlistId = 7, soundId = 15, volume = null),
                            PlaylistXRefSound(playlistId = 7, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 7, soundId = 6, volume = null),

                            PlaylistXRefSound(playlistId = 8, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 8, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 8, soundId = 13, volume = null),

                            PlaylistXRefSound(playlistId = 9, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 9, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 9, soundId = 1, volume = null),

                            PlaylistXRefSound(playlistId = 10, soundId = 12, volume = null),
                            PlaylistXRefSound(playlistId = 10, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 10, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 10, soundId = 5, volume = null),

                            PlaylistXRefSound(playlistId = 11, soundId = 12, volume = null),
                            PlaylistXRefSound(playlistId = 11, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 11, soundId = 6, volume = null),
                            PlaylistXRefSound(playlistId = 11, soundId = 1, volume = null),

                            PlaylistXRefSound(playlistId = 12, soundId = 12, volume = null),
                            PlaylistXRefSound(playlistId = 12, soundId = 9, volume = null),
                            PlaylistXRefSound(playlistId = 12, soundId = 15, volume = null),

                            PlaylistXRefSound(playlistId = 13, soundId = 4, volume = null),
                            PlaylistXRefSound(playlistId = 13, soundId = 6, volume = null),
                            PlaylistXRefSound(playlistId = 13, soundId = 5, volume = null),

                            PlaylistXRefSound(playlistId = 14, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 14, soundId = 5, volume = null),
                            PlaylistXRefSound(playlistId = 14, soundId = 1, volume = null),

                            PlaylistXRefSound(playlistId = 15, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 15, soundId = 12, volume = null),
                            PlaylistXRefSound(playlistId = 15, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 15, soundId = 8, volume = null),

                            PlaylistXRefSound(playlistId = 16, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 16, soundId = 10, volume = null),
                            PlaylistXRefSound(playlistId = 16, soundId = 5, volume = null),

                            PlaylistXRefSound(playlistId = 17, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 17, soundId = 7, volume = null),
                            PlaylistXRefSound(playlistId = 17, soundId = 3, volume = null),

                            PlaylistXRefSound(playlistId = 18, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 18, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 18, soundId = 3, volume = null),


                            PlaylistXRefSound(playlistId = 19, soundId = 3, volume = null),
                            PlaylistXRefSound(playlistId = 19, soundId = 4, volume = null),


                            PlaylistXRefSound(playlistId = 20, soundId = 10, volume = null),
                            PlaylistXRefSound(playlistId = 20, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 20, soundId = 7, volume = null),


                            PlaylistXRefSound(playlistId = 21, soundId = 4, volume = null),
                            PlaylistXRefSound(playlistId = 21, soundId = 9, volume = null),
                            PlaylistXRefSound(playlistId = 21, soundId = 13, volume = null),

                            PlaylistXRefSound(playlistId = 22, soundId = 6, volume = null),
                            PlaylistXRefSound(playlistId = 22, soundId = 1, volume = null),
                            PlaylistXRefSound(playlistId = 22, soundId = 7, volume = null),

                            PlaylistXRefSound(playlistId = 23, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 23, soundId = 9, volume = null),
                            PlaylistXRefSound(playlistId = 23, soundId = 11, volume = null),

                            PlaylistXRefSound(playlistId = 24, soundId = 8, volume = null),
                            PlaylistXRefSound(playlistId = 24, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 24, soundId = 6, volume = null),

                            PlaylistXRefSound(playlistId = 25, soundId = 5, volume = null),
                            PlaylistXRefSound(playlistId = 25, soundId = 9, volume = null),

                            PlaylistXRefSound(playlistId = 26, soundId = 3, volume = null),
                            PlaylistXRefSound(playlistId = 26, soundId = 9, volume = null),
                            PlaylistXRefSound(playlistId = 26, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 26, soundId = 13, volume = null),

                            PlaylistXRefSound(playlistId = 27, soundId = 14, volume = null),
                            PlaylistXRefSound(playlistId = 27, soundId = 2, volume = null),
                            PlaylistXRefSound(playlistId = 27, soundId = 9, volume = null),
                            PlaylistXRefSound(playlistId = 27, soundId = 12, volume = null),

                            PlaylistXRefSound(playlistId = 28, soundId = 6, volume = null),
                            PlaylistXRefSound(playlistId = 28, soundId = 9, volume = null),


                            )
                    }

                    soundDaoProvider.get().run {
                        insert(
                            Sound(
                                id = 1,
                                name = context.getString(R.string.forest_sound_name),
                                isPaid = true
                            ),
                            Sound(
                                id = 2,
                                name = context.getString(R.string.train_station_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 3,
                                name = context.getString(R.string.river_in_the_woods_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 4,
                                name = context.getString(R.string.rain_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 5,
                                name = context.getString(R.string.night_owl_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 6,
                                name = context.getString(R.string.underwater_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 7,
                                name = context.getString(R.string.wood_fire_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 8,
                                name = context.getString(R.string.birds_whistle_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 9,
                                name = context.getString(R.string.seawash_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 10,
                                name = context.getString(R.string.medieval_sound_name),
                                isPaid = true
                            ),
                            Sound(
                                id = 11,
                                name = context.getString(R.string.blizzard_sound_name),
                                isPaid = true
                            ),
                            Sound(
                                id = 12,
                                name = context.getString(R.string.farm_sound_name),
                                isPaid = true
                            ),
                            Sound(
                                id = 13,
                                name = context.getString(R.string.office_sound_name),
                                isPaid = true
                            ),
                            Sound(
                                id = 14,
                                name = context.getString(R.string.traffic_sound_name),
                                isPaid = false
                            ),
                            Sound(
                                id = 15,
                                name = context.getString(R.string.waterfall_sound_name),
                                isPaid = false
                            ),
                        )
                    }
                }
            }
        }).fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun providePlaylistDao(database: ZenisoundsDatabase): PlaylistDao = database.playlistDao

    @Provides
    @Singleton
    fun provideSoundDao(database: ZenisoundsDatabase): SoundDao = database.soundDao

    @Provides
    @Singleton
    fun providePlaylistXRevSoundDao(database: ZenisoundsDatabase): PlaylistXRefSoundDao =
        database.playlistXRefSoundDao

    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContextProvider = CoroutineContextProvider()

    @Provides
    @Singleton
    fun provideActivePlaylist(): ActivePlaylistProvider = ActivePlaylistProvider()

    @Provides
    @Singleton
    fun provideLocationService(okHttpClient: OkHttpClient): GeolocationService =
        createWebService(okHttpClient, locationBaseUrl)

    @Provides
    @Singleton
    fun provideWeatherService(okHttpClient: OkHttpClient): WeatherService =
        createWebService(okHttpClient, weatherApiBaseUrl)


    private inline fun <reified T> createWebService(
        okHttpClient: OkHttpClient,
        url: String
    ): T {
        val jsonConverter = Json {
            encodeDefaults = true
            ignoreUnknownKeys = true
            isLenient = true
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(
                jsonConverter.asConverterFactory(
                    "application/json".toMediaType()
                )
            )
            .client(okHttpClient)
            .build()
        return retrofit.create(T::class.java)
    }

    @Provides
    @Singleton
    fun provideOkHttpBasicClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else HttpLoggingInterceptor.Level.NONE
        return OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    private const val locationBaseUrl = "http://ip-api.com/"
    private const val weatherApiBaseUrl = "https://api.openweathermap.org/"
}