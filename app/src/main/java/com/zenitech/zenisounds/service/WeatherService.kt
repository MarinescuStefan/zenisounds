package com.zenitech.zenisounds.service

import com.zenitech.zenisounds.BuildConfig
import com.zenitech.zenisounds.model.LocationDTO
import com.zenitech.zenisounds.model.WeatherAPIResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("data/2.5/forecast")
    suspend fun getWeather(
        @Query("lat") latitude: Float,
        @Query("lon") longitude: Float,
        @Query("appid") apiKey: String = BuildConfig.OPEN_WEATHER_API_KEY,
        @Query("units") units: String = "metric"
    ) : WeatherAPIResponse
}