package com.zenitech.zenisounds.service

import com.zenitech.zenisounds.model.LocationDTO
import retrofit2.http.GET

interface GeolocationService {

    @GET("json")
    suspend fun getUserLocation() : LocationDTO
}