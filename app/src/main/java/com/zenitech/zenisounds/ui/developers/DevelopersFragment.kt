package com.zenitech.zenisounds.ui.developers

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.zenitech.zenisounds.MainActivity
import com.zenitech.zenisounds.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DevelopersFragment : Fragment(R.layout.fragment_developers) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val decorView = activity!!.window.decorView
//        decorView.systemUiVisibility = (
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
//        val view: View = view.findViewById(R.id.root)
//        view.fitsSystemWindows = true
//        view.setPadding(0, 0, 0, 0)
        (activity as AppCompatActivity).setSupportActionBar((activity as MainActivity).findViewById(R.id.toolbar))
        (activity as AppCompatActivity).supportActionBar?.show()
//        (activity as MainActivity).findViewById<DrawerLayout>(R.id.drawer_layout).apply {
//            fitsSystemWindows = true
//            setPadding(0, 0, 0, 0)
//        }
    }

}