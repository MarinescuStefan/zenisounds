package com.zenitech.zenisounds.ui.playlist

import android.view.DragEvent
import android.view.DragEvent.ACTION_DRAG_ENTERED
import com.zenitech.zenisounds.databinding.SoundItemLayoutBinding


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zenitech.zenisounds.database.dao.SoundUiModel
import com.zenitech.zenisounds.util.Util

class SoundAdapter(private val onVolumeChangeListener: (VolumeChange) -> Unit) :
    ListAdapter<SoundUiModel, SoundAdapter.SoundViewHolder>(SoundDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SoundViewHolder {
        val binding =
            SoundItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SoundViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SoundViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item)
    }

    inner class SoundViewHolder(
        private val binding: SoundItemLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SoundUiModel) {
            binding.sbSound.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    onVolumeChangeListener.invoke(
                        VolumeChange(
                            item.sound.id,
                            progress.toFloat() / 10
                        )
                    )
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

                override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit

            })
            val image = Util.getSoundImage(item.sound.id)
            binding.ivSoundPhoto.setImageResource(image)
            binding.tvSoundName.text = item.sound.name
            binding.sbSound.progress = (item.volume!! * 10).toInt()
        }
    }

    object SoundDiffUtil : DiffUtil.ItemCallback<SoundUiModel>() {
        override fun areItemsTheSame(
            oldItem: SoundUiModel,
            newItem: SoundUiModel
        ): Boolean = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: SoundUiModel,
            newItem: SoundUiModel
        ): Boolean = oldItem == newItem
    }


}

data class VolumeChange(
    val soundId: Int,
    val volume: Float
)