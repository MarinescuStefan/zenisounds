package com.zenitech.zenisounds.ui.main

import androidx.annotation.DrawableRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.zenitech.zenisounds.usecase.MoodUseCase
import com.zenitech.zenisounds.usecase.PlaylistUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val moodUseCase: MoodUseCase,
    private val playlistUseCase: PlaylistUseCase,
) : ViewModel() {

    private val channel = Channel<Int>()
    val moodPlaylist = channel.receiveAsFlow()

    fun onMoodBoosterClicked() = viewModelScope.launch {
        val mood = moodUseCase.mood.get()
        val playlistId = playlistUseCase.getPlayListForMood(mood)
        channel.send(playlistId)
    }

    val playlists = liveData(viewModelScope.coroutineContext) {
        emit(playlistUseCase.getPlaylists())
    }

    val mood = liveData(viewModelScope.coroutineContext) {
        emit(moodUseCase.getMood())
    }

}

data class MoodUiModel(
    val title: String,
    @DrawableRes
    val icon: Int
)