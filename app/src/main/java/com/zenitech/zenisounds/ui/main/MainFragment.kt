package com.zenitech.zenisounds.ui.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.navigation.NavigationView
import com.zenitech.zenisounds.MainActivity
import com.zenitech.zenisounds.R
import com.zenitech.zenisounds.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import timber.log.Timber

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val playlistAdapter = PlaylistAdapter {
            findNavController().navigate(
                MainFragmentDirections.actionMainFragmentToPlaylistFragment(
                    it
                )
            )
        }
        val binding = FragmentMainBinding.bind(view).apply {

            btnMoodBooster.setOnClickListener {
                viewModel.onMoodBoosterClicked()
            }

            recyclerView.apply {
                adapter = playlistAdapter
                layoutManager =
                    StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL).apply {
                        gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
                    }
                setHasFixedSize(true)
            }

            (activity as AppCompatActivity).run {
                supportActionBar?.hide()
                setSupportActionBar(toolbar)
            }

            val activity = activity as MainActivity
            val appBarConfiguration = AppBarConfiguration(
                setOf(R.id.playlistFragment),
                activity.findViewById<DrawerLayout>(R.id.drawer_layout)
            )
            activity.findViewById<NavigationView>(R.id.navigation_view)
                .setupWithNavController(findNavController())
            val navController =
                (activity.supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment).navController
            setupActionBarWithNavController(
                (activity as AppCompatActivity),
                navController,
                appBarConfiguration
            )
        }

        lifecycleScope.launchWhenStarted {
            viewModel.moodPlaylist.collect {
                findNavController().navigate(
                    MainFragmentDirections.actionMainFragmentToPlaylistFragment(
                        it
                    )
                )
            }
        }

        viewModel.playlists.observe(viewLifecycleOwner, {
            Timber.d("Size of list ${it.size}")
            playlistAdapter.submitList(it)
        })

        viewModel.mood.observe(viewLifecycleOwner, {
            binding.ivPhoto.setImageResource(it.icon)
            binding.toolbar.title = "test"
            (activity as AppCompatActivity).supportActionBar?.title = it.title

            //   (activity as AppCompatActivity).supportActionBar?.title = it.data.title
//                    (activity as AppCompatActivity).run {
//                        titleColor = R.color.colorOnPrimary
//                        title =  it.data.title
//                    }

        })

    }

}
