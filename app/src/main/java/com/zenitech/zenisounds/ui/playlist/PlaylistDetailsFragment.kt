package com.zenitech.zenisounds.ui.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player.REPEAT_MODE_ALL
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.zenitech.zenisounds.R
import com.zenitech.zenisounds.databinding.FragmentPlaylistDetailsLayoutBinding
import com.zenitech.zenisounds.provider.ActivePlaylistProvider
import com.zenitech.zenisounds.util.Util
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlaylistDetailsFragment : BottomSheetDialogFragment() {

    private val viewModel: PlaylistDetailsViewModel by viewModels()

    @Inject
    lateinit var activePlaylistProvider: ActivePlaylistProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activePlaylistProvider.stopPlayer()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_playlist_details_layout, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundAdapter = SoundAdapter {
            activePlaylistProvider.changeVolume(it)
        }
        val binding = FragmentPlaylistDetailsLayoutBinding.bind(view).apply {
            rvSounds.apply {
                adapter = soundAdapter
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
            }
        }

        viewModel.playlistDetails.observe(viewLifecycleOwner, {
            binding.tvPlaylistName.text = it.playlistName
            binding.ivPlaylistStart.apply {
                setImageResource(R.drawable.ic_pause)

                setOnClickListener {
                    setImageResource(
                        if (activePlaylistProvider.isPlayerRunning()) {
                            R.drawable.ic_play
                        } else R.drawable.ic_pause
                    )
                    activePlaylistProvider.toggleStartStop()
                }
            }

            it.sounds.run {
                soundAdapter.submitList(this)
                forEach {
                    val mediaSource = buildMediaSource(it.sound.id)
                    val player = SimpleExoPlayer.Builder(requireContext()).build().apply {
                        setMediaSource(mediaSource)
                        volume = it.volume!!
                        repeatMode = REPEAT_MODE_ALL
                        playWhenReady = true
                        prepare()
                    }
                    activePlaylistProvider.addPlayer(it.sound.id,player)
                }
            }
        })

    }

    private fun buildMediaSource(soundId: Int): MediaSource {
        val rawResourceDataSource = RawResourceDataSource(requireContext())
        val sound = Util.getSound(soundId)
        val uri = RawResourceDataSource.buildRawResourceUri(sound)
        val x = rawResourceDataSource.open(DataSpec(uri))
        val mediaItem = MediaItem.fromUri(rawResourceDataSource.uri!!)
        val mediaSource =
            ProgressiveMediaSource.Factory { rawResourceDataSource }.createMediaSource(mediaItem)
        return mediaSource
    }


}