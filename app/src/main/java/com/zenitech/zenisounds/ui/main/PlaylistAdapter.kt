package com.zenitech.zenisounds.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zenitech.zenisounds.database.entity.Playlist
import com.zenitech.zenisounds.databinding.PlaylistItemLayoutBinding
import com.zenitech.zenisounds.util.Util

class PlaylistAdapter(private val onClickListener: (Int) -> Unit) :
    ListAdapter<Playlist, PlaylistAdapter.PlaylistViewHolder>(PlaylistDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val binding =
            PlaylistItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item)
    }

    inner class PlaylistViewHolder(
        private val binding: PlaylistItemLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Playlist?) {

            item?.let { item ->
                val params: ViewGroup.LayoutParams = binding.clParent.layoutParams
                params.height = (2..3).random() * 300
                binding.clParent.layoutParams = params
                binding.tvPlaylistName.text = item.name

                item.color?.let {
                    binding.clParent.setBackgroundColor(it)
                }

                item.image?.let {
                    binding.ivPlaylist.setBackgroundResource(Util.getPlaylistImage(it))
                }

                itemView.setOnClickListener {
                    onClickListener.invoke(item.id)
                }
            }
        }
    }

    object PlaylistDiffCallback : DiffUtil.ItemCallback<Playlist>() {
        override fun areItemsTheSame(
            oldItem: Playlist,
            newItem: Playlist
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Playlist,
            newItem: Playlist
        ): Boolean = oldItem == newItem
    }


}