package com.zenitech.zenisounds.ui.playlist

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.zenitech.zenisounds.usecase.PlaylistDetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlaylistDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val playlistDetailsUseCase: PlaylistDetailsUseCase
) : ViewModel() {
    private val playlistId: Int? = savedStateHandle.get<Int>("id")


    val playlistDetails = liveData(viewModelScope.coroutineContext) {
        playlistId?.let {
            emit(playlistDetailsUseCase.getPlaylistSounds(it))
        }
    }
}