package com.zenitech.zenisounds.database.dao

import androidx.room.*
import com.zenitech.zenisounds.database.entity.PlaylistXRefSound
import com.zenitech.zenisounds.database.entity.Sound

@Dao
interface PlaylistXRefSoundDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg playlist: PlaylistXRefSound)

//    @Transaction
//    @Query("SELECT * FROM Playlist WHERE id =:playlistId")
//    suspend fun getPlaylistDetails(playlistId: Int): PlaylistWithSounds

    @Transaction
    @Query("SELECT xref.volume as volume,Sound.* FROM (SELECT * FROM PlaylistXRefSound WHERE PlaylistXRefSound.playlist_id =:playlistId  ) as xref  INNER JOIN Sound on Sound.id = xref.sound_id")
    suspend fun getPlaylistDetails(playlistId: Int): List<SoundUiModel>

    @Query("SELECT Playlist.name FROM PlayList WHERE Playlist.id =:playlistId")
    suspend fun getPlaylistName(playlistId: Int): String
}

data class SoundUiModel(
    @Embedded
    val sound: Sound,
    val volume: Float?
)