package com.zenitech.zenisounds.database.entity

import androidx.room.*

@Entity(primaryKeys = ["playlist_id","sound_id"])
data class PlaylistXRefSound(
    @ColumnInfo(name = "playlist_id")
    val playlistId: Int,
    @ColumnInfo(name = "sound_id")
    val soundId: Int,
    val volume: Float?
)

data class PlaylistWithSounds(
    @Embedded
    val playlist: Playlist,
    @Relation(
        parentColumn = "id",
        entity = Sound::class,
        entityColumn = "id",
        associateBy = Junction(
            value = PlaylistXRefSound::class,
            parentColumn = "playlist_id",
            entityColumn = "sound_id"
        )
    )
    val sounds: List<Sound>
)