package com.zenitech.zenisounds.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zenitech.zenisounds.database.entity.Playlist
import com.zenitech.zenisounds.database.entity.PlaylistType
import javax.inject.Singleton

@Dao
interface PlaylistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(playlist: Playlist)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(playlists: List<Playlist>)

    @Query("SELECT * FROM Playlist WHERE Playlist.type IN (:types)")
    suspend fun getPlaylists(
        types: List<PlaylistType> = listOf(
            PlaylistType.DEFAULT,
            PlaylistType.CUSTOM
        )
    ): List<Playlist>

    @Query("SELECT Playlist.id FROM Playlist WHERE Playlist.name =:mood AND Playlist.type =:type")
    suspend fun getPLaylistForMood(mood: String, type: PlaylistType = PlaylistType.MOOD): Int

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insert(playlists: List<Playlist>)
}