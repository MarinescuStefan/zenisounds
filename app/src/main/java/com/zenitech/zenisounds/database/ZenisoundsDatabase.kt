package com.zenitech.zenisounds.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zenitech.zenisounds.database.dao.PlaylistDao
import com.zenitech.zenisounds.database.dao.PlaylistXRefSoundDao
import com.zenitech.zenisounds.database.dao.SoundDao
import com.zenitech.zenisounds.database.entity.Playlist
import com.zenitech.zenisounds.database.entity.PlaylistXRefSound
import com.zenitech.zenisounds.database.entity.Sound

@Database(entities = [Playlist::class, Sound::class,PlaylistXRefSound::class], version = 1,exportSchema = true)
abstract class ZenisoundsDatabase : RoomDatabase() {

    abstract val playlistDao: PlaylistDao
    abstract val soundDao: SoundDao
    abstract val playlistXRefSoundDao: PlaylistXRefSoundDao
}