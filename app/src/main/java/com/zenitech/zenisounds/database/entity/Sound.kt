package com.zenitech.zenisounds.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Sound(
    @PrimaryKey
    val id: Int,
    val name: String,
    val isPaid: Boolean
)
