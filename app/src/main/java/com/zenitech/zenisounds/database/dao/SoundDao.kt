package com.zenitech.zenisounds.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.zenitech.zenisounds.database.entity.PlaylistXRefSound
import com.zenitech.zenisounds.database.entity.Sound
import javax.inject.Singleton

@Dao
interface SoundDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg playlist: Sound)
}