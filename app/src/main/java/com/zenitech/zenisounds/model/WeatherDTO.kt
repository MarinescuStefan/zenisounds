package com.zenitech.zenisounds.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherAPIResponse(
    @SerialName("list")
    val weathers: List<WeatherList>
)

@Serializable
data class WeatherList(
    @SerialName("main") val main: MainWeather,
    @SerialName("weather") val weather: List<WeatherDTO>

)

@Serializable
data class WeatherDTO(val id: Int, val icon: String)

@Serializable
data class MainWeather(
    @SerialName("temp") val temperature: Float
)
