package com.zenitech.zenisounds.model

import kotlinx.serialization.*

@Serializable
data class LocationDTO(
    @SerialName("lat")
    val latitude : Float?,
    @SerialName("lon")
    val longitude : Float?
)