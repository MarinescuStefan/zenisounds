package com.zenitech.zenisounds

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.zenitech.zenisounds.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)
            setSupportActionBar(toolbar)
            toolbar.setTitleTextColor(resources.getColor(R.color.colorOnPrimary))
            initNavigation(this)
        }
        //status bar
//        window.decorView.systemUiVisibility = (
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)


//        //app bar
//        getWindow().setFlags(
//            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
//            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

      //  getSupportActionBar()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

//        supportActionBar?.apply {
//            setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
//        }
//
        window?.apply {
            statusBarColor = resources.getColor(R.color.colorPrimary)
            navigationBarColor = resources.getColor(R.color.colorPrimary)
        }
//        supportActionBar?.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
    }

    private fun initNavigation(activityMainBinding: ActivityMainBinding) {
        Timber.d("init navigation")
        navController = (supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment).navController
        activityMainBinding.navigationView.itemIconTintList = null
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            val x = 10

        }
        appBarConfiguration = AppBarConfiguration(setOf(R.id.playlistFragment),activityMainBinding.drawerLayout)
        activityMainBinding.navigationView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment_container)
        return navController.navigateUp(appBarConfiguration) ||
                super.onSupportNavigateUp()
    }
}